const pokedex$$ = document.querySelector(".pokedex");
const search$$ = document.querySelector(".input");
const typeSelector$$ = document.querySelectorAll(".types__selector");
console.log(typeSelector$$);
const urlPokemon = "https://pokeapi.co/api/v2/pokemon/?limit=251&offset=0";
//La primera función llama a la Api y recupera los primeros 151 pokemon.
let ALL_POKEMONS = [];
const typeColors = {
  fire: "#FF4500",
  grass: "#006400",
  water: "#00FFFF",
  flying: "#F4A460",
  electric: "#FFD700",
  ground: "#8B4513",
  rock: "#C0C0C0",
  fairy: "#FFB6C1",
  bug: "#98FB98",
  poison: "#BA55D3",
  dragon: "#663399",
  psychic: "#FF69B4",
  fighting: "#A52A2A",
  normal: "#FFDAB9",
  ghost: "#800080",
  steel: "#708090",
  ice: "#ADD8E6",
  dark: "#191970",
}

const callApi = async () => {
  const dataApi = await fetch(urlPokemon);
  const dataApiJSON = await dataApi.json();
  ALL_POKEMONS = dataApiJSON.results;
  ;

  const mainTypes = Object.keys(typeColors);

  // console.log(mainTypes);

  search$$.addEventListener("input", () => search(dataApiJSON));
  // console.log(dataApiJSON);
  paint(dataApiJSON);
  
};


const paint = async (pokemons) => {
  pokedex$$.innerHTML = ``;
  for (const pokemon of pokemons.results) {
    const pokmnCard$$ = document.createElement("div");
    pokmnCard$$.className = "pkmnCard";
    const dataApiFicha = await fetch(pokemon.url);
    const infoPokemon = await dataApiFicha.json();
// console.log(infoPokemon);
    if (infoPokemon.types[1]) {
      pokmnCard$$.innerHTML = `

          <img class="pkmn__image" src="${
            infoPokemon.sprites.front_default
          }" alt="${infoPokemon.name}" />
          <h2 class="pkmn__name">${infoPokemon.name}</h2>
          <div class="pkmn__info">
            <p class ="pkmn__info--id">${infoPokemon.id
              .toString()
              .padStart(3, "0")}</p>
            <p class ="pkmn__info--measure">Altura: ${infoPokemon.height}</p>
            <p class ="pkmn__info--measure">Peso: ${infoPokemon.weight}</p>
            <p class ="pkmn__info--type">${infoPokemon.types[0].type.name}</p>
            <p class ="pkmn__info--type">${infoPokemon.types[1].type.name}</p>
          </div>
      `;

      pokedex$$.appendChild(pokmnCard$$);
    } else {
      pokmnCard$$.innerHTML = `

          <img class="pkmn__image" src="${
            infoPokemon.sprites.front_default
          }" alt="${infoPokemon.name}" />
          <h2 class="pkmn__name">${infoPokemon.name}</h2>
          <div class="pkmn__info">
          <p class ="pkmn__info--id">${infoPokemon.id
            .toString()
            .padStart(3, "0")}</p>
            <p class ="pkmn__info--measure">Altura: ${infoPokemon.height}</p>
            <p class ="pkmn__info--measure">Peso: ${infoPokemon.weight}</p>
            <p class ="pkmn__info--type">${infoPokemon.types[0].type.name}</p>
          </div>
      `;

      pokedex$$.appendChild(pokmnCard$$);
    }
  }
};

const search = (pokemons) => {
  const pokemonFilter = [];

  pokemons.results.forEach((pokemon) => {
    if (
      pokemon.name.toLowerCase().includes(search$$.value.toLowerCase().trim())
    ) {
      pokemonFilter.push(pokemon);
    }
  });
  paint({ results: pokemonFilter });
  //LE ESTAMOS PASANDO UN OBJETO CON ATRIBUTO RESULTS PORQUE LA FUNCIÓN PAINT LO NECESITA
  //pokemonFilter es un array, por lo que la guardamos dentro de un objeto results
};

//Necesitamos una función por la que al hacer click en uno de los contenedores con clase de tipo de pokemon pinte solo los pokemon que tienen ese tipo.
//Para ello localizamos los tipos dentro de la segunda url. ¿Se puede hacer dentro de la función paint o hay que volver a hacer fetch?

const filterPokemonByType = (type) => {
  document.body.style.backgroundColor = typeColors[type];
}

typeSelector$$.forEach(button => {
  button.addEventListener("click", (event) => {
     
      filterPokemonByType(event.target.classList[1]);
      
  })
});
// const typeClick = (pokemons) => {
//   for (const pokemon of pokemons.results) {
//     const dataApiFicha = fetch(pokemon.url);
//     const infoPokemon = dataApiFicha.json();
//     const typeFilter = [];
//     if (infoPokemon.types.includes(typeSelector$$.className)) {
//       typeFilter.push(pokemon);
//     }

//     console.log(typeSelector$$);
//   }
//   paint({ results: typeFilter });

// };

callApi();

//LIMPIAR ARRAY ANTES DE BUSCA
